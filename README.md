lxbattery is a battery monitor for laptops which lives in the system tray.

While it's intended to work best with lxde, it should work well with any modern
desktop.

BUILDING
--------

 $ make

DEBUG
-----

 $ make debug

This will create lxbattery-debug.

 $ ./lxbattery-debug

One can run this and echo values into the local sys/ directory to test the system.
