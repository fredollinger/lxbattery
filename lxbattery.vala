using Gtk;

public class Main {

  class LxBattery : Window {
    private StatusIcon trayicon;
    private DBusProxy batprox;

    private string BatteryInfoRootPath;
    private string status;
    private int power;

    public LxBattery() {

#if DEBUG_SYS
      BatteryInfoRootPath = "sys";
#else
      BatteryInfoRootPath = "/sys/bus/acpi/drivers/battery/PNP0C0A:00/power_supply/BAT0";
#endif
      status = "";
      power = -1;

      string path = "/reyv/resources/battery-icon.jpg";
      Gdk.Pixbuf image = new Gdk.Pixbuf.from_resource(path);

      trayicon = new StatusIcon.from_pixbuf(image);
      trayicon.set_visible(true);

      trayicon.activate.connect(about_clicked);

      create_menuSystem();

      update_power();
      start_timer();
      trayicon.popup_menu.connect(menuSystem_popup);

      // https://wiki.gnome.org/HowDoI/GtkApplication
      var n = new GLib.Notification ("lxbattery");
      GLib.Application.get_default ().send_notification ("lxbattery", n);
    } // END LxBattery()

    public string send (string summary, string body, string? id_suffix = null) {
       var n = new GLib.Notification (summary);
       n.set_body (body);

       string id = "lxbattery-%s".printf (id_suffix ?? "");

       /* Default action: Bring the account window to the front */
       //n.set_default_action_and_target_value ("app.show-window", account.id);

       GLib.Application.get_default().send_notification (id, n);

       return id;
     }

    string GetFileAsString (string str) {

        var file = File.new_for_path (str);
        string res = "";

        if (!file.query_exists ()) {
            stderr.printf ("File '%s' doesn't exist.\n", file.get_path ());
            return res;
        }

        try {
            // Open file for reading and wrap returned FileInputStream into a
            // DataInputStream, so we can read line by line
            var dis = new DataInputStream (file.read ());
            string line;
            // Read lines until end of file (null) is reached
            while ((line = dis.read_line (null)) != null) {
                res = res + line;
                // stdout.printf ("[%s]\n", line);
            }
        } catch (Error e) {
            error ("%s", e.message);
        }

        return res;
    } // END GetFileAsString()

    int GetFileAsInt(string str) {

        var file = File.new_for_path (str);

        if (!file.query_exists ()) {
            stderr.printf ("File '%s' doesn't exist.\n", file.get_path ());
            return 1;
        }

        try {
            // Open file for reading and wrap returned FileInputStream into a
            // DataInputStream, so we can read line by line
            var dis = new DataInputStream (file.read ());
            string line;
            // Read lines until end of file (null) is reached
            while ((line = dis.read_line (null)) != null) {
                // stdout.printf ("[%s]\n", line);
                return int.parse(line);
            }
        } catch (Error e) {
            error ("%s", e.message);
        }

        return -1;
    } // END GetFileAsInt()

    public bool update_power() {
        float num = GetFileAsInt(BatteryInfoRootPath + "/energy_now");
        float denom = GetFileAsInt(BatteryInfoRootPath + "/energy_full");
        power = (int)((num / denom) * 100);

        string new_status = GetFileAsString(BatteryInfoRootPath + "/status");

        trayicon.set_tooltip_text ("Battery\n \n Power: " + power.to_string() + "% \n Status: " + new_status);

        if (status != new_status && "Discharging" == new_status) {
            // TODO: If status changes show notification
            // http://i-hate-farms.github.io/parrot/gio-2.0/GLib.Application.send_notification.html
            // stdout.printf ("Status changed [%s]\n", new_status);
            Gtk.MessageDialog msg = new Gtk.MessageDialog (this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK, new_status);
            msg.response.connect ((response_id) => {
                msg.destroy();
            }); // END msg.response.connect
            msg.show();
        }
        status = new_status;

        return true;
    } // END update_power()

    public void start_timer() {
        Timeout.add(1000, (SourceFunc)update_power);
    }

    /* Create menu for right button */
    public void create_menuSystem() {
      // The "widget" is the menu that was supplied when
      // g_signal_connect_swapped() was called.
      //menuSystem = GTK_MENU (widget);
      // menuSystem = new Menu();
      var menuAbout = new ImageMenuItem.from_stock(Stock.ABOUT, null);
      menuAbout.activate.connect(about_clicked);
      // menuSystem.append(menuAbout);
      var menuQuit = new ImageMenuItem.from_stock(Stock.QUIT, null);
      menuQuit.activate.connect(Gtk.main_quit);
      // menuSystem.append(menuQuit);
      // menuSystem.show_all();
    }

    /* Show popup menu on right button */
    private void menuSystem_popup(uint button, uint time) {
      // menuSystem.popup(null, null, null, button, time);
    }

    private void about_clicked() {
      var about = new AboutDialog();
      about.set_version("0.0.1");
      about.set_program_name("lxbattery");
      about.set_comments("Battery Utility");
      about.set_copyright("Frederick Ollinger 2016");
      about.run();
      about.hide();
    }
  }

  public static int main (string[] args) {
    Gtk.init(ref args);
    var App = new LxBattery();
    App.hide();
    Gtk.main();
    return 0;
  }

} // END class Main
