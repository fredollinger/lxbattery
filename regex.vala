using Gtk;

class Demo : GLib.Object {

    public Demo() {
    }

    int GetFileAsInt(string str) {

        var file = File.new_for_path (str);

        if (!file.query_exists ()) {
            stderr.printf ("File '%s' doesn't exist.\n", file.get_path ());
            return 1;
        }

        try {
            // Open file for reading and wrap returned FileInputStream into a
            // DataInputStream, so we can read line by line
            var dis = new DataInputStream (file.read ());
            string line;
            // Read lines until end of file (null) is reached
            while ((line = dis.read_line (null)) != null) {
                stdout.printf ("[%s]\n", line);
                return int.parse(line);
            }
        } catch (Error e) {
            error ("%s", e.message);
        }

        return -1;
    } // END GetFileAsInt()

        int GetBatteryPercent(string str) {
            Regex regex = /percentage/;
            var x2=str.split("\n");

            MatchInfo info;

            foreach ( var x3 in x2 ) {
                //stdout.printf(x3);

                if(regex.match(x3, 0, out info)) {
                    var x4=x3.split("percentage:");
                    // TODO NOW GET PERCENTAGE OUT OF THE LINE
                    //stdout.printf(info);
                    stdout.printf("[%s] \n", x4[1]);
                }
            }
            return 0;
    }

    public static int main(string[] args) {
    var demo = new Demo();

    string bat_info = """
      native-path:          BAT0
      vendor:               ASUSTeK
      model:                ASUS Battery
      power supply:         yes
      updated:              Mon 12 Dec 2016 06:38:06 AM PST (47 seconds ago)
      has history:          yes
      has statistics:       yes
      battery
        present:             yes
        rechargeable:        yes
        state:               discharging
        warning-level:       none
        energy:              26.615 Wh
        energy-empty:        0 Wh
        energy-full:         28.819 Wh
        energy-full-design:  37.331 Wh
        energy-rate:         8.808 W
        voltage:             7.6 V
        time to empty:       3.0 hours
        percentage:          92%
        capacity:            77.1986%
        technology:          lithium-ion
        icon-name:          'battery-full-symbolic'
      History (rate):
        1481553486	8.808	discharging
    """;

        // stdout.printf(bat_info);
        //stdout.printf("%i \n", demo.GetBatteryPercent(bat_info));
        stdout.printf("[%i] \n", demo.GetFileAsInt("/sys/bus/acpi/drivers/battery/PNP0C0A:00/power_supply/BAT0/energy_full"));
        return 0;
    }
}
