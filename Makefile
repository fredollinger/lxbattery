APP=lxbattery
TARGET=reyv
RESOURCE=$(TARGET)-resource.xml

all: resources.c
	valac --pkg=gtk+-3.0 resources.c $(APP).vala -o $(APP)
clean:
	rm -f $(APP) resources.c

resources.c:
	glib-compile-resources --target=resources.c --generate-source $(RESOURCE)

regex:
	valac --pkg=gtk+-3.0 $@.vala -o $@

install:
	mkdir -p $(DESTDIR)/usr/bin
	cp $(APP) $(DESTDIR)/usr/bin

debug: resources.c
	valac -D DEBUG_SYS --pkg=gtk+-3.0 resources.c $(APP).vala -o $(APP)-debug
